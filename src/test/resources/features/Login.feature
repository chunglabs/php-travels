Feature: Login
  Scenario: login FrontEnd with existed account
    Given that Chung opened the FrontEnd Login Page
    When he login with existed account
      | email               | password |
      | user@phptravels.com | demouser |
    Then he should be able to view his account profile

  Scenario Outline: Login with invalid account
    Given that Chung opened the FrontEnd Login Page
    When he login with invalid "<email>" or "<password>"
    Then The message "Invalid Email or Password" will be show
    Examples:

      | email                | password        |
      |                      | demouser        |
      | user@phptravels.coms |                 |
      | notExistedEmail      | password        |
      | user@phptravels.com  | invalidPassword |
