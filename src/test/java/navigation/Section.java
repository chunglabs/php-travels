package navigation;

public enum  Section {
    FrontEnd("https://www.phptravels.net"),
    BackEnd("https://www.phptravels.net/admin"),
    FrontEndLoginPage("https://www.phptravels.net/login")
    ;
    private final String url;

    Section(String url) {
        this.url = url;
    }

    public String url() {
        return url;
    }
}
