package login;

import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import navigation.Navigate;
import navigation.Section;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;
import static net.serenitybdd.screenplay.questions.WebElementQuestion.the;
import static org.hamcrest.CoreMatchers.equalTo;

public class LoginDefs {
    @Before
    public void set_The_Stage(){
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("^that (.*) opened the FrontEnd Login Page$")
    public void something_opened_the_frontend_login_page(String actorName) throws Throwable {
       theActorCalled(actorName).attemptsTo(
               Navigate.to(Section.FrontEndLoginPage)
       );
    }

    @When("^he login with existed account$")
    public void he_login_with_existed_account(DataTable credentials) throws Throwable {
        theActorInTheSpotlight().attemptsTo(
            Login.with(credentials)
        );
    }
    @Then("^he should be able to view his account profile$")
    public void he_should_be_able_to_view_his_profile() throws Throwable{
        theActorInTheSpotlight().should(
                seeThat(the(LoginForm.Account_Profile_Tab),isVisible())
        );
    }
    @When("^he login with invalid \"([^\"]*)\" or \"([^\"]*)\"$")
    public void he_login_with_invalid_or(String email, String password) throws Throwable {
        theActorInTheSpotlight().attemptsTo(
            Login.with(Authenticate.with(email,password))
        );
    }

    @Then("^The message \"([^\"]*)\" will be show$")
    public void the_message_something_will_be_show(String message) throws Throwable {
        theActorInTheSpotlight().should(seeThat(LoginAlert.displayed(),equalTo(message)));
    }

}
