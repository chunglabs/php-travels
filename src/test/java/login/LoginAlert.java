package login;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class LoginAlert implements Question<String> {
    @Override
    public String answeredBy(Actor actor) {
        return Text.of(LoginForm.LOGIN_ARLERT).viewedBy(actor).asString();
    }

    public static Question<String> displayed(){
        return new LoginAlert();
    }
}
