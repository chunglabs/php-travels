package login;

import cucumber.api.DataTable;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.thucydides.core.annotations.Step;

import java.util.List;
import java.util.Map;

public class Login implements Task {
    private DataTable credentials;
    private Authenticate authenticate;

    public Login(DataTable credentials) {
        this.credentials = credentials;
    }

    public Login(Authenticate authenticate) {
        this.authenticate = authenticate;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(authenticate.Email()).into(LoginForm.EMAIL_FIELD),
                Enter.theValue(authenticate.Password()).into(LoginForm.PASSWORD_FIELD),
                Click.on(LoginForm.LOGIN_BTN)
        );
    }

    public static Login with(DataTable credentials){
        return new Login(Authenticate.with(credentials));
    }
    public static Login with(Authenticate authenticate){
        return new Login(authenticate);
    }
}
