package login;

import cucumber.api.DataTable;
import net.serenitybdd.screenplay.Ability;
import net.serenitybdd.screenplay.Actor;

import java.util.List;
import java.util.Map;

public class Authenticate implements Ability {
    private final String email;
    private final String password;

    public Authenticate(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String Email() {
        return email;
    }

    public String Password() {
        return password;
    }

    public static Authenticate with(DataTable credentialsData) {
        List<Map<String, String>> credentials = credentialsData.asMaps(String.class, String.class);
        return new Authenticate(credentials.get(0).get("email"), credentials.get(0).get("password"));
    }

    public static Authenticate with(String email, String password) {
        return new Authenticate(email, password);
    }
}
