package login;

import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.pages.PageObject;


public class LoginForm extends PageObject {
    public static Target EMAIL_FIELD = Target.the("email field")
            .locatedBy("//*[@id=\"loginfrm\"]/div[1]/div[5]/div/div[1]/input");
    public static Target PASSWORD_FIELD = Target.the("password field")
            .locatedBy("//*[@id=\"loginfrm\"]/div[1]/div[5]/div/div[2]/input");
    public static Target LOGIN_BTN = Target.the("login button")
            .locatedBy("#loginfrm > button");
    public static Target LOGIN_ARLERT = Target.the("login alert")
            .locatedBy("#loginfrm > div.panel.panel-default > div.resultlogin > div");
    public static Target Account_Profile_Tab = Target.the("account profile tab")
            .locatedBy("#body-section > div.container > div.container.mt25.offset-0 > div > div.col-md-1.offset-0 > ul > li.active > a");
    public static Target REMEMBERME_CHECKBOX = Target.the("remember me checkbok")
            .locatedBy("");
}
